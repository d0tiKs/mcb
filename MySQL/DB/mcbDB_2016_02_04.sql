-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: my_contact_book
-- ------------------------------------------------------
-- Server version	5.5.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_contact`
--

DROP TABLE IF EXISTS `com_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contact` int(11) NOT NULL,
  `id_com_type` int(11) NOT NULL,
  `val` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`val`),
  KEY `fk_com_contact_contact1_idx` (`id_contact`),
  KEY `fk_com_contact_com_type1_idx` (`id_com_type`),
  CONSTRAINT `fk_com_contact_com_type1` FOREIGN KEY (`id_com_type`) REFERENCES `com_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_com_contact_contact1` FOREIGN KEY (`id_contact`) REFERENCES `contact` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='That table contains the different value to contact someone, each value is linked to a com_type that defines the tpye of communication.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_contact`
--

LOCK TABLES `com_contact` WRITE;
/*!40000 ALTER TABLE `com_contact` DISABLE KEYS */;
INSERT INTO `com_contact` VALUES (1,1,1,'01.23.45.67.89'),(2,1,3,'dupont.jean@unknown.com'),(3,2,2,'nintendo.mario'),(4,3,2,'nintendo.luigi');
/*!40000 ALTER TABLE `com_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_type`
--

DROP TABLE IF EXISTS `com_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_UNIQUE` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Table that contains each different way to contact someone, contained as a string';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_type`
--

LOCK TABLES `com_type` WRITE;
/*!40000 ALTER TABLE `com_type` DISABLE KEYS */;
INSERT INTO `com_type` VALUES (4,'Address'),(3,'E-mail'),(2,'Skype'),(1,'Tel.');
/*!40000 ALTER TABLE `com_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uq_name` varchar(10) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Forname` varchar(45) NOT NULL,
  `id_company` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_name_UNIQUE` (`uq_name`),
  KEY `fk_contact_contact_companies_idx` (`id_company`),
  CONSTRAINT `fk_contact_contact_companies` FOREIGN KEY (`id_company`) REFERENCES `contact_companies` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1 COMMENT='Reference each contact';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'dupont_j','Dupont','Jean',NULL),(2,'ninten_m','Nintendo','Mario',1),(3,'ninten_l','Nintendo','Luigi',1);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_companies`
--

DROP TABLE IF EXISTS `contact_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Name_UNIQUE` (`Name`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Reference each companies';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_companies`
--

LOCK TABLES `contact_companies` WRITE;
/*!40000 ALTER TABLE `contact_companies` DISABLE KEYS */;
INSERT INTO `contact_companies` VALUES (1,'Mario Bros','Japan',NULL);
/*!40000 ALTER TABLE `contact_companies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-04 10:48:25
