using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Text;

namespace MCB_Client.SerialModels
{
	public partial class SRcontact
	{
		public string uq_name { get; set; }
		public string Name { get; set; }
		public string Forname { get; set; }
		public Nullable<int> id_company { get; set; }

		public SRcontact(string uqn, string name, string forname, Nullable<int> idc)
		{
			this.uq_name = uqn;
			this.Name = name;
			this.Forname = forname;
			this.id_company = idc;
		}

		public string SRContact_JsonSerialized()
		{
			return JsonConvert.SerializeObject(this);
		}
	}

	public class SRContactList : Collection<SRcontact>
	{
		public SRContactList()
		{

		}

		public SRContactList(IList<SRcontact> contacts)
			: base(contacts)
		{

		}

		public string SRContactList_JsonSerialized()
		{
			return JsonConvert.SerializeObject(this);
		}
	}
}

