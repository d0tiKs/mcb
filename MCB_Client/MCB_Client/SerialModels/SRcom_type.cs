using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Text;

namespace MCB_Client.SerialModels
{
		public enum ENUMcom_types
		{
			tel = 1,
			skype = 2,
			mail = 3,
			address = 4
		};

	public class SRcom_type
	{
		public string type { get; set; }

		public SRcom_type(string type)
		{
			this.type = type;
		}
	}

	public class SRCom_typeList : Collection<SRcom_type>
	{
		public SRCom_typeList()
		{

		}

		public SRCom_typeList(IList<SRcom_type> com_types)
			: base(com_types)
		{

		}

		public override string ToString()
		{
			StringBuilder sb_ctl = new StringBuilder();
			foreach(SRcom_type srct in this)
			{
				sb_ctl.AppendLine(srct.type.ToString());
			}

			return sb_ctl.ToString();
		}
	}
}
