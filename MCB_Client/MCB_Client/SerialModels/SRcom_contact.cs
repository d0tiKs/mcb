using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Text;

namespace MCB_Client.SerialModels
{
	public class SRcom_contact
	{
		public int id_contact { get; set; }
		public int id_com_type { get; set; }
		public string val { get; set; }

		public SRcom_contact(int id_contact, int id_com_type, string val)
		{
			this.id_contact = id_contact;
			this.id_com_type = id_com_type;
			this.val = val;
		}
	}

	public class SRcom_contactList : Collection<SRcom_contact>
	{
		public SRcom_contactList()
		{

		}

		public SRcom_contactList(IList<SRcom_contact> com_contacts)
			: base(com_contacts)
		{

		}


	}
}
