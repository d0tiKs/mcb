using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace MCB_Client.SerialModels
{
	public partial class SRcontact_company
	{
		public string Name { get; set; }
		public string Country { get; set; }
		public string Address { get; set; }

		public SRcontact_company(string uqn, string name, string country, string address)
		{
			this.Name = name;
			this.Country = country;
			this.Address = address;
		}
	}

	public class SRContact_companiesList : Collection<SRcontact_company>
	{
		public SRContact_companiesList()
		{

		}

		public SRContact_companiesList(IList<SRcontact_company> contacts_companies)
			: base(contacts_companies)
		{

		}
	}
}
