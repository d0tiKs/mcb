﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;

using MCB_Client.CustomForms;
using MCB_Client.SerialModels;

namespace MCB_ClientExe
{
	public partial class MCB_Client : Form
	{
		public MCB_Client()
		{
			InitializeComponent();
		}

		private void bt_schcontact_Click(object sender, EventArgs e)
		{
			string result = "";
			string request = "";
			int id = 0;
			string uqn = "";
			CustomMessageBox.ShowDialog("Search Contact", "Choose the way to find the contact", "By id", "by UQN", ref result);

			if (result == "By id")
			{
				CustomMessageBox.ShowDialog("Search Contact by id", "Enter the contact's id", "Request", ref result, ref id);

				request = @"http://localhost:55823/RequestTools/BasicRequest.svc/getcontactbyid/" + id;
			}
			else if (result == "by UQN")
			{
				CustomMessageBox.ShowDialog("Search Contact by id", "Enter the contact's uq_name", "Request", ref result, ref uqn);
				request = @"http://localhost:55823/RequestTools/BasicRequest.svc/getcontactbyuqn/" + uqn;
			}
			else
				return;

			using (var client = new HttpClient())
			{
				var getContact = new HttpRequestMessage(HttpMethod.Get, request);
				var result2 = client.SendAsync(getContact).Result;
				var strResult = result2.Content.ReadAsStringAsync().Result;

				Console.WriteLine(strResult);
			}
		}

		private void bt_addcontact_Click(object sender, EventArgs e)
		{
			string uqn = "";
			string name = "";
			string forname = "";
			string result = "";

			CustomMessageBox.ShowDialog("Delete Contact", "Enter the contact's name", "OK", ref result, ref name);
			if (result != "OK")
				return;
			CustomMessageBox.ShowDialog("Delete Contact", "Enter the contact's forname", "OK", ref result, ref forname);
			if (result != "OK")
				return;
			CustomMessageBox.ShowDialog("Delete Contact", "Enter the contact's uq_name", "OK", ref result, ref uqn);
			if (result != "OK")
				return;

			SRcontact sct = new SRcontact(uqn, name, forname, null);

			using (var client = new HttpClient())
			{
				var createContact = new HttpRequestMessage(HttpMethod.Post, "http://localhost:55823/RequestTools/BasicRequest.svc/addcontact");


				createContact.Content = new StringContent(sct.SRContact_JsonSerialized());
				createContact.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

				var requestResult = client.SendAsync(createContact).Result;
				var strResult = requestResult.Content.ReadAsStringAsync().Result;

				Console.WriteLine(strResult);
			}
		}

		private void bt_schcom_Click(object sender, EventArgs e)
		{
			string result = "";
			string request = "http://localhost:55823/RequestTools/BasicRequest.svc";
			string uqn = "";
			string com_type = "";

			CustomMessageBox.ShowDialog("Seach Communication For One Contact", "Enter the contact's uq_name", "OK", ref result, ref uqn);
			CustomMessageBox.ShowDialog("Search Communication", "Please choose the type of search", "Specific Communication", "All Communications", ref result);

			if (result == "Specific Communication")
			{
					request += @"/getspeccomcontactbyuqn/" + uqn;
					CustomMessageBox.ShowDialog("Seach Communication For One Contact", "Select the com_type", "Request", new[] { "tel", "skype", "mail", "address" }, ref result, ref com_type);
					request += @"/" + com_type;
			}
			else if (result == "All Communications")
			{
				request += @"/getcomcontactbyuqn/" + uqn;
			}
			else
				return;

			using (var client = new HttpClient())
			{
				var getComtype = new HttpRequestMessage(HttpMethod.Get, request);
				var requestResult = client.SendAsync(getComtype).Result;
				var resultlist = requestResult.Content.ReadAsStringAsync().Result;

				Console.WriteLine(resultlist);
			}
		}

		private void bt_delcontact_Click(object sender, EventArgs e)
		{
			string uqn = "";
			string result = "";

			CustomMessageBox.ShowDialog("Delete Contact", "Enter the contact's uq_name", "OK", ref result, ref uqn);

			SRcontact sct = new SRcontact(uqn, "test", "truc", null);

			if (result != "OK")
				return;

			using (var client = new HttpClient())
			{
				var deleteContact = new HttpRequestMessage(HttpMethod.Post, "http://localhost:55823/RequestTools/BasicRequest.svc/deletecontact");

				deleteContact.Content = new StringContent(sct.SRContact_JsonSerialized());
				deleteContact.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

				var requestResult = client.SendAsync(deleteContact).Result;
				var strResult = requestResult.Content.ReadAsStringAsync().Result;

				Console.WriteLine(strResult);
			}
		}
	}
}
