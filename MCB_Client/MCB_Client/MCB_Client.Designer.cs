﻿namespace MCB_ClientExe
{
	partial class MCB_Client
	{
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MCB_Client));
			this.bt_schcontact = new System.Windows.Forms.Button();
			this.bt_schcom = new System.Windows.Forms.Button();
			this.bt_addcontact = new System.Windows.Forms.Button();
			this.bt_delcontact = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// bt_schcontact
			// 
			resources.ApplyResources(this.bt_schcontact, "bt_schcontact");
			this.bt_schcontact.Name = "bt_schcontact";
			this.bt_schcontact.UseVisualStyleBackColor = true;
			this.bt_schcontact.Click += new System.EventHandler(this.bt_schcontact_Click);
			// 
			// bt_schcom
			// 
			resources.ApplyResources(this.bt_schcom, "bt_schcom");
			this.bt_schcom.Name = "bt_schcom";
			this.bt_schcom.UseVisualStyleBackColor = true;
			this.bt_schcom.Click += new System.EventHandler(this.bt_schcom_Click);
			// 
			// bt_addcontact
			// 
			resources.ApplyResources(this.bt_addcontact, "bt_addcontact");
			this.bt_addcontact.Name = "bt_addcontact";
			this.bt_addcontact.UseVisualStyleBackColor = true;
			this.bt_addcontact.Click += new System.EventHandler(this.bt_addcontact_Click);
			// 
			// bt_delcontact
			// 
			resources.ApplyResources(this.bt_delcontact, "bt_delcontact");
			this.bt_delcontact.Name = "bt_delcontact";
			this.bt_delcontact.UseVisualStyleBackColor = true;
			this.bt_delcontact.Click += new System.EventHandler(this.bt_delcontact_Click);
			// 
			// MCB_Client
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.bt_delcontact);
			this.Controls.Add(this.bt_addcontact);
			this.Controls.Add(this.bt_schcom);
			this.Controls.Add(this.bt_schcontact);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.Name = "MCB_Client";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button bt_schcontact;
		private System.Windows.Forms.Button bt_schcom;
		private System.Windows.Forms.Button bt_addcontact;
		private System.Windows.Forms.Button bt_delcontact;

	}
}

