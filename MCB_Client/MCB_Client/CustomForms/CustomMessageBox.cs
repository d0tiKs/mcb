﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCB_Client.CustomForms
{
	public static class CustomMessageBox
	{
		public static void ShowDialog(string title, string txtmsg, string bt1t, string bt2t, ref string bresult)
		{
			Form prompt = new Form();

			Button bt1 = new Button();
			Button bt2 = new Button();
			Button bt3 = new Button();
			Button bt4 = new Button();

			Label lb_txtmsg = new Label();
			TextBox tb_entry = new TextBox();

			string r = "";

			InitializeComponent(bt1, bt2, bt3, bt4, lb_txtmsg, tb_entry);

			prompt.Text = title;
			lb_txtmsg.Text = txtmsg;

			bt1.Text = bt1t;
			bt1.Name = bt1t;
			bt1.Invalidate();

			bt2.Text = bt2t;
			bt2.Name = bt2t;

			bt4.Text = "Cancel";
			bt4.Name = "Cancel";

			bt3.Enabled = false;
			bt3.Visible = false;

			tb_entry.Enabled = false;
			tb_entry.Visible = false;

			bt1.Click += (sender, e) => { r = bt1.Name; prompt.Close(); };
			bt2.Click += (sender, e) => { r = bt2.Name; prompt.Close(); };
			bt3.Click += (sender, e) => { r = bt3.Name; prompt.Close(); };
			bt4.Click += (sender, e) => { r = bt4.Name; prompt.Close(); };

			prompt.Controls.Add(bt1);
			prompt.Controls.Add(bt2);
			prompt.Controls.Add(bt3);
			prompt.Controls.Add(bt4);
			prompt.Controls.Add(lb_txtmsg);
			prompt.Controls.Add(tb_entry);

			prompt.Size = new System.Drawing.Size(320, 110);
			prompt.FormBorderStyle = FormBorderStyle.FixedToolWindow;

			prompt.ShowDialog();
			bresult = r;
		}

		public static void ShowDialog(string title, string txtmsg, string bt1t, ref string bresult, ref int id)
		{
			Form prompt = new Form();

			Button bt1 = new Button();
			Button bt2 = new Button();
			Button bt3 = new Button();
			Button bt4 = new Button();

			Label lb_txtmsg = new Label();
			TextBox tb_entry = new TextBox();

			string r = "";
			int tid = 0;

			InitializeComponent(bt1, bt2, bt3, bt4, lb_txtmsg, tb_entry);

			lb_txtmsg.Text = txtmsg;
			lb_txtmsg.Location = new System.Drawing.Point(15, 10);

			bt1.Text = bt1t;
			bt1.Name = bt1t;
			bt1.Location = new System.Drawing.Point(215, 15);

			bt2.Enabled = false;
			bt2.Visible = false;

			bt3.Enabled = false;
			bt3.Visible = false;

			bt4.Text = "Cancel";
			bt4.Name = "Cancel";

			tb_entry.Enabled = true;
			tb_entry.Visible = true;
			tb_entry.Location = new System.Drawing.Point(15, 40);

			bt1.Click += (sender, e) => { r = bt1.Name; prompt.Close(); };
			bt2.Click += (sender, e) => { r = bt2.Name; prompt.Close(); };
			bt3.Click += (sender, e) => { r = bt3.Name; prompt.Close(); };
			bt4.Click += (sender, e) => { r = bt4.Name; prompt.Close(); };

			tb_entry.KeyPress += (sender, e) => {
				if ((!Int32.TryParse(e.KeyChar.ToString(), out tid) && tb_entry.Text != ""))
				{
					e.KeyChar = '\0';
					tid = 0;
				}
			};

			prompt.Controls.Add(bt1);
			prompt.Controls.Add(bt2);
			prompt.Controls.Add(bt3);
			prompt.Controls.Add(bt4);
			prompt.Controls.Add(lb_txtmsg);
			prompt.Controls.Add(tb_entry);

			prompt.Size = new System.Drawing.Size(320, 110);
			prompt.FormBorderStyle = FormBorderStyle.FixedToolWindow;

			prompt.ShowDialog();
			bresult = r;
			id = tid;
		}

		public static void ShowDialog(string title, string txtmsg, string bt1t, ref string bresult, ref string uqn)
		{
			Form prompt = new Form();

			Button bt1 = new Button();
			Button bt2 = new Button();
			Button bt3 = new Button();
			Button bt4 = new Button();

			Label lb_txtmsg = new Label();
			TextBox tb_entry = new TextBox();

			string r = "";

			InitializeComponent(bt1, bt2, bt3, bt4, lb_txtmsg, tb_entry);

			prompt.Text = title;

			lb_txtmsg.Text = txtmsg;
			lb_txtmsg.Location = new System.Drawing.Point(15, 10);

			bt1.Text = bt1t;
			bt1.Name = bt1t;
			bt1.Location = new System.Drawing.Point(215, 15);

			bt2.Enabled = false;
			bt2.Visible = false;

			bt3.Enabled = false;
			bt3.Visible = false;

			bt4.Text = "Cancel";
			bt4.Name = "Cancel";

			tb_entry.Enabled = true;
			tb_entry.Visible = true;
			tb_entry.Location = new System.Drawing.Point(15, 40);


			bt1.Click += (sender, e) => { if(tb_entry.Text!=null){ r = bt1.Name; prompt.Close(); }};
			bt4.Click += (sender, e) => { r = bt4.Name; prompt.Close(); };

			prompt.Controls.Add(bt1);
			prompt.Controls.Add(bt2);
			prompt.Controls.Add(bt3);
			prompt.Controls.Add(bt4);
			prompt.Controls.Add(lb_txtmsg);
			prompt.Controls.Add(tb_entry);

			prompt.Size = new System.Drawing.Size(320, 110);
			prompt.FormBorderStyle = FormBorderStyle.FixedToolWindow;

			prompt.ShowDialog();
			bresult = r;
			uqn = tb_entry.Text;
		}

		public static void ShowDialog(string title, string txtmsg, string bt1t, string[] choices_cb, ref string bresult, ref string selection)
		{
			Form prompt = new Form();

			Button bt1 = new Button();
			Button bt2 = new Button();
			Button bt3 = new Button();
			Button bt4 = new Button();

			Label lb_txtmsg = new Label();
			ComboBox cb_choices = new ComboBox();

			string r = "";

			InitializeComponent(bt1, bt2, bt3, bt4, lb_txtmsg, cb_choices);

			prompt.Text = title;

			lb_txtmsg.Text = txtmsg;
			lb_txtmsg.Location = new System.Drawing.Point(15, 10);

			bt1.Text = bt1t;
			bt1.Name = bt1t;
			bt1.Location = new System.Drawing.Point(215, 15);

			bt2.Enabled = false;
			bt2.Visible = false;

			bt3.Enabled = false;
			bt3.Visible = false;

			bt4.Text = "Cancel";
			bt4.Name = "Cancel";

			cb_choices.Enabled = true;
			cb_choices.Visible = true;
			cb_choices.Location = new System.Drawing.Point(15, 40);

			foreach(string c in choices_cb)
			{
				cb_choices.Items.Add(c);
			}

			bt1.Click += (sender, e) => { if (cb_choices.SelectedItem != null) { r = bt1.Name; prompt.Close(); } };
			bt4.Click += (sender, e) => { r = bt4.Name; prompt.Close(); };

			prompt.Controls.Add(bt1);
			prompt.Controls.Add(bt2);
			prompt.Controls.Add(bt3);
			prompt.Controls.Add(bt4);
			prompt.Controls.Add(lb_txtmsg);
			prompt.Controls.Add(cb_choices);

			prompt.Size = new System.Drawing.Size(320, 110);
			prompt.FormBorderStyle = FormBorderStyle.FixedToolWindow;

			prompt.ShowDialog();
			bresult = r;
			selection = cb_choices.Text;
		}


		internal static  void InitializeComponent(Button bt1, Button bt2, Button bt3, Button bt4, Label lb_txtmsg, TextBox tb_entry)
		{
			// 
			// bt1
			// 
			bt1.Location = new System.Drawing.Point(15, 12);
			bt1.Name = "bt1";
			bt1.Size = new System.Drawing.Size(75, 23);
			bt1.TabIndex = 0;
			bt1.Text = "button1";
			bt1.UseVisualStyleBackColor = true;

			// 
			// bt2
			// 
			bt2.Location = new System.Drawing.Point(96, 12);
			bt2.Name = "bt2";
			bt2.Size = new System.Drawing.Size(75, 23);
			bt2.TabIndex = 1;
			bt2.Text = "button2";
			bt2.UseVisualStyleBackColor = true;

			// 
			// bt3
			// 
			bt3.Location = new System.Drawing.Point(215, 15);
			bt3.Name = "bt3";
			bt3.Size = new System.Drawing.Size(75, 22);
			bt3.TabIndex = 2;
			bt3.Text = "button3";
			bt3.UseVisualStyleBackColor = true;

			// 
			// bt4
			// 
			bt4.Location = new System.Drawing.Point(215, 40);
			bt4.Name = "bt4";
			bt4.Size = new System.Drawing.Size(75, 25);
			bt4.TabIndex = 3;
			bt4.Text = "button4";
			bt4.UseVisualStyleBackColor = true;

			// 
			// lb_txtmsg
			// 
			lb_txtmsg.AutoSize = true;
			lb_txtmsg.Location = new System.Drawing.Point(12, 48);
			lb_txtmsg.Name = "lb_txtmsg";
			lb_txtmsg.Size = new System.Drawing.Size(35, 13);
			lb_txtmsg.TabIndex = 4;
			lb_txtmsg.Text = "label1";
			// 
			// cb_choices
			// 
			tb_entry.Location = new System.Drawing.Point(55, 45);
			tb_entry.Name = "tb_entry";
			tb_entry.Size = new System.Drawing.Size(145, 20);
			tb_entry.TabIndex = 5;
		}

		internal static void InitializeComponent(Button bt1, Button bt2, Button bt3, Button bt4, Label lb_txtmsg, ComboBox cb_choices)
		{
			// 
			// bt1
			// 
			bt1.Location = new System.Drawing.Point(15, 12);
			bt1.Name = "bt1";
			bt1.Size = new System.Drawing.Size(75, 23);
			bt1.TabIndex = 0;
			bt1.Text = "button1";
			bt1.UseVisualStyleBackColor = true;


			// 
			// bt2
			// 
			bt2.Location = new System.Drawing.Point(96, 12);
			bt2.Name = "bt2";
			bt2.Size = new System.Drawing.Size(75, 23);
			bt2.TabIndex = 1;
			bt2.Text = "button2";
			bt2.UseVisualStyleBackColor = true;

			// 
			// bt3
			// 
			bt3.Location = new System.Drawing.Point(215, 15);
			bt3.Name = "bt3";
			bt3.Size = new System.Drawing.Size(75, 22);
			bt3.TabIndex = 2;
			bt3.Text = "button3";
			bt3.UseVisualStyleBackColor = true;

			// 
			// bt4
			// 
			bt4.Location = new System.Drawing.Point(215, 40);
			bt4.Name = "bt4";
			bt4.Size = new System.Drawing.Size(75, 25);
			bt4.TabIndex = 3;
			bt4.Text = "button4";
			bt4.UseVisualStyleBackColor = true;

			// 
			// lb_txtmsg
			// 
			lb_txtmsg.AutoSize = true;
			lb_txtmsg.Location = new System.Drawing.Point(12, 48);
			lb_txtmsg.Name = "lb_txtmsg";
			lb_txtmsg.Size = new System.Drawing.Size(35, 13);
			lb_txtmsg.TabIndex = 4;
			lb_txtmsg.Text = "label1";
			// 
			// cb_choices
			// 
			cb_choices.Location = new System.Drawing.Point(55, 45);
			cb_choices.Name = "tb_entry";
			cb_choices.Size = new System.Drawing.Size(145, 20);
			cb_choices.TabIndex = 5;
		}
	}
}
