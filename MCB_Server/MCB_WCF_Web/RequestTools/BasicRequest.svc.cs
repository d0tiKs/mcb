﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

using Newtonsoft.Json;

using MCB_DBlib.Models;
using MCB_WCF_Web.SerialModels;

namespace MCB_WCF_Web.RequestTools
{
	public class BasicRequest : IBasicRequest
	{
		#region Contacts

		public SRContactList getContactList()
		{
			SRContactList sctl = new SRContactList();
			SRcontact sct;
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			foreach (contact ct in mcbCtx.contacts.ToList())
			{
				sct = new SRcontact(ct.id, ct.uq_name, ct.Name, ct.Forname, ct.id_company);
				sctl.Add(sct);
			}
			return sctl;
		}

		public SRcontact getContactByid(string sid)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			contact ct = null;
			SRcontact sct = null;
			int id;

			if (int.TryParse(sid, out id))
			{
				try
				{
					ct = mcbCtx.contacts.Find(id);

					sct = new SRcontact(ct.id, ct.uq_name, ct.Name, ct.Forname, ct.id_company);

					if (ct == null)
						throw new MissingFieldException();

					return sct;
				}
				catch (Exception ex)
				{

					//TODO LOG
				}
			}
			else
				throw new Exception("The parameter \"sid\" has to be an Interger");
			//TODO LOG

			return sct;
		}

		public SRcontact getContactByUqn(string uqn)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			contact ct = null;
			SRcontact sct = null;

			try
			{
				ct = mcbCtx.contacts.First(c => c.uq_name == uqn);

				sct = new SRcontact(ct.id, ct.uq_name, ct.Name, ct.Forname, ct.id_company);

				if (ct == null)
					throw new MissingFieldException();

				return sct;
			}
			catch (Exception ex)
			{

				//TODO LOG
			}

			return sct;
		}

		public void addContact(string uqn, string name, string forname)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();
			try
			{
				mcbCtx.contacts.Add(new contact(uqn, name, forname));
				mcbCtx.SaveChanges();
			}
			catch (Exception)
			{
				//Todo LOG
			}
		}

		public SRcontact addContact(SRcontact contact)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();
			try
			{
				mcbCtx.contacts.Add(new contact(contact.uq_name, contact.Name, contact.Forname));
				mcbCtx.SaveChanges();

				return getContactByUqn(contact.uq_name);
			}
			catch (Exception)
			{
				//Todo LOG
			}

			return null;
		}

		public void deleteContactByid(string sid)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			contact ct_to_del = null;
			int id;

			if (int.TryParse(sid, out id))
			{
				try
				{
					ct_to_del = mcbCtx.contacts.Find(id);

					if (ct_to_del != null)
						mcbCtx.contacts.Remove(ct_to_del);

					mcbCtx.SaveChanges();
				}
				catch (Exception)
				{
					//Todo LOG
				}
			}
			else
				throw new Exception("The parameter \"sid\" has to be an Interger");
			//TODO LOG
		}

		public void deleteContactByUqn(string uqn)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			contact ct_to_del = null;
			try
			{
				ct_to_del = mcbCtx.contacts.First(c => c.uq_name == uqn);

				if (ct_to_del != null)
					mcbCtx.contacts.Remove(ct_to_del);

				mcbCtx.SaveChanges();

			}
			catch (Exception)
			{
				//Todo LOG
			}
		}

		public string deleteContact(SRcontact contact)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			contact ct_to_del = null;
			try
			{
				ct_to_del = mcbCtx.contacts.First(c => c.uq_name == contact.uq_name);

				if (ct_to_del != null)
					mcbCtx.contacts.Remove(ct_to_del);

				mcbCtx.SaveChanges();
				return "Contact "+ contact.uq_name + " deleted";

			}
			catch (Exception)
			{
				//Todo LOG
			}
			return "Unknown Error";
		}
		#endregion

		#region Communication

		public SRcom_contactList getComContactByid(string sid)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			contact ct;
			SRcom_contactList sctl = new SRcom_contactList();
			int id;

			if (int.TryParse(sid, out id))
			{
				try
				{
					ct = mcbCtx.contacts.Find(id);
					foreach (com_contact cc in ct.com_contact)
					{
						sctl.Add(new SRcom_contact(cc.id, cc.id_contact, cc.id_com_type, cc.val));
					}

					return sctl;
				}
				catch (Exception)
				{
					//Todo LOG
				}
			}
			else
				throw new Exception("The parameter \"sid\" has to be an Interger");
			//TODO LOG
			return sctl;
		}

		public SRcom_contactList getComContactByUqn(string uqn)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			contact ct;
			SRcom_contactList sctl = new SRcom_contactList();
			try
			{
				ct = mcbCtx.contacts.First(c => c.uq_name == uqn);
				foreach (com_contact cc in ct.com_contact)
				{
					sctl.Add(new SRcom_contact(cc.id, cc.id_contact, cc.id_com_type, cc.val));
				}

				return sctl;
			}
			catch (Exception)
			{
				//Todo LOG
			}
			return sctl;
		}

		public SRcom_contactList getSpecComContactByid(string sid, string type)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			SRcom_contactList scctl = new SRcom_contactList();

			contact ct;
			ENUMcom_types etype;

			int id;

			if (int.TryParse(sid, out id))
			{

				switch (type)
				{
					case "tel":
						etype = ENUMcom_types.tel;
						break;
					case "skype":
						etype = ENUMcom_types.skype;
						break;
					case "mail":
						etype = ENUMcom_types.mail;
						break;
					case "address":
						etype = ENUMcom_types.address;
						break;
					default:
						throw new Exception("incorrect type, must be a value from [tel, skype, mail, address");
				}

				try
				{
					ct = mcbCtx.contacts.Find(id);
					foreach (com_contact cc in mcbCtx.com_contact.Where(cm => cm.id_contact == ct.id && cm.id_com_type == (int)etype).ToList())
					{
						scctl.Add(new SRcom_contact(cc.id, cc.id_contact, cc.id_com_type, cc.val));
					}
					return scctl;
				}
				catch (Exception)
				{
					//Todo LOG
				}
			}
			else
				throw new Exception("The parameter \"sid\" has to be an Interger");

			return scctl;
		}

		public SRcom_contactList getSpecComContactByUqn(string uqn, string type)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			SRcom_contactList scctl = new SRcom_contactList();

			contact ct;
			ENUMcom_types etype;

			switch (type)
			{
				case "tel":
					etype = ENUMcom_types.tel;
					break;
				case "skype":
					etype = ENUMcom_types.skype;
					break;
				case "mail":
					etype = ENUMcom_types.mail;
					break;
				case "address":
					etype = ENUMcom_types.address;
					break;
				default:
					throw new Exception("incorrect type, must be a value from [tel, skype, mail, address");
			}

			try
			{
				ct = mcbCtx.contacts.First(c => c.uq_name == uqn);
				foreach (com_contact cc in mcbCtx.com_contact.Where(cm => cm.id_contact == ct.id && cm.id_com_type == (int)etype).ToList())
				{
					scctl.Add(new SRcom_contact(cc.id, cc.id_contact, cc.id_com_type, cc.val));
				}
				return scctl;
			}
			catch (Exception)
			{
				//Todo LOG
			}
			return scctl;
		}
		#endregion

		public Dictionary<string, string> getContactByCom_type(string type)
		{
			my_contact_bookContext mcbCtx = new my_contact_bookContext();

			ENUMcom_types etype;

			switch (type)
			{
				case "tel":
					etype = ENUMcom_types.tel;
					break;
				case "skype":
					etype = ENUMcom_types.skype;
					break;
				case "mail":
					etype = ENUMcom_types.mail;
					break;
				case "address":
					etype = ENUMcom_types.address;
					break;
				default:
					throw new Exception("incorrect type, must be a value from [tel, skype, mail, address");
			}

			Dictionary<string, string> dic = new Dictionary<string, string>();
			try
			{
				dic = (from ct in mcbCtx.contacts
							 join cm_ct in mcbCtx.com_contact
							 on ct.id equals cm_ct.id_contact
							 where cm_ct.id_com_type == (int)etype
							 select new
							 {
								 uq_name = ct.uq_name,
								 val = cm_ct.val
							 }).ToDictionary(a => a.uq_name, b => b.val);

				/*Dictionary<string,string> dic = mcbCtx.contacts
																					.Join(mcbCtx.com_contact, c => c.id, cc => cc.id_contact, (c, cc) => new { c, cc })
																					.Where(a => a.cc.id_com_type == (int)etype)
																					.Select(a => new
																					{
																						uq_name = a.c.uq_name,
																						val = a.cc.val
																					})
																					.ToDictionary(a => a.uq_name, b => b.val);*/

				return dic;

			}
			catch (Exception ex)
			{
				//TODO LOG
			}

			return dic;
		}
	}
}
