﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

using MCB_DBlib.Models;
using MCB_WCF_Web.SerialModels;

namespace MCB_WCF_Web.RequestTools
{
	[ServiceContract]
	public interface IBasicRequest
	{
		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "getcontactlist")]
		SRContactList getContactList();

		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "getcontactbyid/{sid}")]
		SRcontact getContactByid(string sid);

		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "getcontactbyuqn/{uqn}")]
		SRcontact getContactByUqn(string uqn);

		/*[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "addcontact/{uqn}/{name}/{forname}")]
		void addContact(string uqn,string name, string forname);*/

		[OperationContract]
		[WebInvoke(Method = "POST",
						RequestFormat = WebMessageFormat.Json,
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "addcontact")]
		SRcontact addContact(SRcontact contact);

		/*[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "deletecontactbyid/{sid}")]
		void deleteContactByid(string sid);

		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "deletecontactbyuqn/{uqn}")]
		void deleteContactByUqn(string uqn);*/

		[OperationContract]
		[WebInvoke(Method = "POST",
						RequestFormat = WebMessageFormat.Json,
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "deletecontact")]
		string deleteContact(SRcontact contact);

		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "getcomcontactbyid/{sid}")]
		SRcom_contactList getComContactByid(string sid);

		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "getcomcontactbyuqn/{uqn}")]
		SRcom_contactList getComContactByUqn(string uqn);

		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "getspeccomcontactbyid/{sid}/{type}")]
		SRcom_contactList getSpecComContactByid(string sid, string type);

		[OperationContract]
		[WebInvoke(Method = "GET",
						ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare,
						UriTemplate = "getspeccomcontactbyuqn/{uqn}/{type}")]
		SRcom_contactList getSpecComContactByUqn(string uqn, string type);
	}
}