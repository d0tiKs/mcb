using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Text;

namespace MCB_WCF_Web.SerialModels
{
	[DataContract(Name = "Contact")]
	public partial class SRcontact
	{
		[DataMember(Order = 1, IsRequired = false, EmitDefaultValue = false)]
		public int id { get; set; }
		[DataMember(Order = 2, IsRequired = true, EmitDefaultValue = false)]
		public string uq_name { get; set; }
		[DataMember(Order = 3, EmitDefaultValue = true)]
		public string Name { get; set; }
		[DataMember(Order = 4, EmitDefaultValue = true)]
		public string Forname { get; set; }
		[DataMember(Order = 5, EmitDefaultValue = true)]
		public Nullable<int> id_company { get; set; }

		public SRcontact(int id, string uqn, string name, string forname, Nullable<int> idc)
		{
			this.id = id;
			this.uq_name = uqn;
			this.Name = name;
			this.Forname = forname;
			this.id_company = idc;
		}
	}

	[CollectionDataContract(Name = "contacts", ItemName = "contact")]
	public class SRContactList : Collection<SRcontact>
	{
		public SRContactList()
		{

		}

		public SRContactList(IList<SRcontact> contacts)
			: base(contacts)
		{

		}
	}
}

