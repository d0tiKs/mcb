using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Text;

namespace MCB_WCF_Web.SerialModels
{
	[DataContract(Name = "Com_Contact")]
	public class SRcom_contact
	{
		[DataMember(Order = 1, IsRequired = true)]
		public int id { get; set; }
		[DataMember(Order = 2, IsRequired = true)]
		public int id_contact { get; set; }
		[DataMember(Order = 3, IsRequired = true)]
		public int id_com_type { get; set; }
		[DataMember(Order = 4, IsRequired = true)]
		public string val { get; set; }

		public SRcom_contact(int id, int id_contact, int id_com_type, string val)
		{
			this.id = id;
			this.id_contact = id_contact;
			this.id_com_type = id_com_type;
			this.val = val;
		}
	}

	[CollectionDataContract(Name = "com_types", ItemName = "com_type")]
	public class SRcom_contactList : Collection<SRcom_contact>
	{
		public SRcom_contactList()
		{

		}

		public SRcom_contactList(IList<SRcom_contact> com_contacts)
			: base(com_contacts)
		{

		}
	}
}
