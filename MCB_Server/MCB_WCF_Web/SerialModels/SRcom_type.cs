using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace MCB_WCF_Web.SerialModels
{
		public enum ENUMcom_types
		{
			tel = 1,
			skype = 2,
			mail = 3,
			address = 4
		};

	[DataContract(Name = "Com_Type")]
	public class SRcom_type
	{
		[DataMember(Order = 1, IsRequired = true, EmitDefaultValue = false)]
		public int id { get; set; }
		[DataMember(Order = 2, IsRequired = true, EmitDefaultValue = false)]
		public string type { get; set; }

		public SRcom_type(int id, string type)
		{
			this.id = id;
			this.type = type;
		}
	}

	[CollectionDataContract(Name = "com_types", ItemName = "com_type")]
	public class SRCom_typeList : Collection<SRcom_type>
	{
		public SRCom_typeList()
		{

		}

		public SRCom_typeList(IList<SRcom_type> com_types)
			: base(com_types)
		{

		}
	}
}
