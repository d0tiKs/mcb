using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace MCB_WCF_Web.SerialModels
{
	[DataContract(Name = "Contact_Company")]
	public partial class SRcontact_company
	{
		[DataMember(Order = 1, IsRequired = true, EmitDefaultValue = false)]
		public int id { get; set; }
		[DataMember(Order = 2, IsRequired = true, EmitDefaultValue = false)]
		public string Name { get; set; }
		[DataMember(Order = 3, EmitDefaultValue = true)]
		public string Country { get; set; }
		[DataMember(Order = 4, EmitDefaultValue = true)]
		public string Address { get; set; }

		public SRcontact_company(int id, string uqn, string name, string country, string address)
		{
			this.id = id;
			this.Name = name;
			this.Country = country;
			this.Address = address;
		}
	}

	[CollectionDataContract(Name = "contact_companies", ItemName = "contact_company")]
	public class SRContact_companiesList : Collection<SRcontact_company>
	{
		public SRContact_companiesList()
		{

		}

		public SRContact_companiesList(IList<SRcontact_company> contacts_companies)
			: base(contacts_companies)
		{

		}
	}
}
