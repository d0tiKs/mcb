using System;
using System.Collections.Generic;

namespace MCB_DBlib.Models
{
	public partial class contact_companies
	{
		public contact_companies()
		{
			this.contacts = new List<contact>();
		}

		public int id { get; set; }
		public string Name { get; set; }
		public string Country { get; set; }
		public string Address { get; set; }
		public virtual ICollection<contact> contacts { get; set; }
	}
}
