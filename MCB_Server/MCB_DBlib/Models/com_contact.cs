using System;
using System.Collections.Generic;
using System.Text;

namespace MCB_DBlib.Models
{
	public partial class com_contact
	{
		public int id { get; set; }
		public int id_contact { get; set; }
		public int id_com_type { get; set; }
		public string val { get; set; }
		public virtual com_type com_type { get; set; }
		public virtual contact contact { get; set; }

		public override string ToString()
		{
			StringBuilder comString = new StringBuilder();

			comString.Append("Com. : ");
			comString.Append("\tid_contact : " + this.id_contact);
			comString.Append("\tcontact : " + this.contact.uq_name);
			comString.Append("\tid_com_type : " + this.id_com_type);
			comString.Append("\tcontact : " + this.com_type.type);
			comString.Append("\tval : " + this.val);

			comString.AppendLine();

			return comString.ToString();
		}
	}
}
