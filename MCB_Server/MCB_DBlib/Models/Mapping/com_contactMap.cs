using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MCB_DBlib.Models.Mapping
{
	public class com_contactMap : EntityTypeConfiguration<com_contact>
	{
		public com_contactMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.val)
					.IsRequired()
					.HasMaxLength(45);

			// Table & Column Mappings
			this.ToTable("com_contact", "my_contact_book");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.id_contact).HasColumnName("id_contact");
			this.Property(t => t.val).HasColumnName("val");
			this.Property(t => t.id_com_type).HasColumnName("id_com_type");

			// Relationships
			this.HasRequired(t => t.com_type)
					.WithMany(t => t.com_contact)
					.HasForeignKey(d => d.id_com_type);
			this.HasRequired(t => t.contact)
					.WithMany(t => t.com_contact)
					.HasForeignKey(d => d.id_contact);

		}
	}
}
