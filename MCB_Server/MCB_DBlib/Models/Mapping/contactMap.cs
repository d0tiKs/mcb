using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MCB_DBlib.Models.Mapping
{
	public class contactMap : EntityTypeConfiguration<contact>
	{
		public contactMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.uq_name)
					.IsRequired()
					.HasMaxLength(10);

			this.Property(t => t.Name)
					.HasMaxLength(45);

			this.Property(t => t.Forname)
					.IsRequired()
					.HasMaxLength(45);

			// Table & Column Mappings
			this.ToTable("contact", "my_contact_book");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.uq_name).HasColumnName("uq_name");
			this.Property(t => t.Name).HasColumnName("Name");
			this.Property(t => t.Forname).HasColumnName("Forname");
			this.Property(t => t.id_company).HasColumnName("id_company");

			// Relationships
			this.HasOptional(t => t.contact_company)
					.WithMany(t => t.contacts)
					.HasForeignKey(d => d.id_company);

		}
	}
}
