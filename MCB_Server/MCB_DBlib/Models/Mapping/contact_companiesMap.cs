using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MCB_DBlib.Models.Mapping
{
	public class contact_companiesMap : EntityTypeConfiguration<contact_companies>
	{
		public contact_companiesMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.Name)
					.IsRequired()
					.HasMaxLength(45);

			this.Property(t => t.Country)
					.HasMaxLength(45);

			this.Property(t => t.Address)
					.HasMaxLength(45);

			// Table & Column Mappings
			this.ToTable("contact_companies", "my_contact_book");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.Name).HasColumnName("Name");
			this.Property(t => t.Country).HasColumnName("Country");
			this.Property(t => t.Address).HasColumnName("Address");
		}
	}
}
