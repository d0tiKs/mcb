using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MCB_DBlib.Models.Mapping
{
	public class com_typeMap : EntityTypeConfiguration<com_type>
	{
		public com_typeMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.type)
					.IsRequired()
					.HasMaxLength(20);

			// Table & Column Mappings
			this.ToTable("com_type", "my_contact_book");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.type).HasColumnName("type");
		}
	}
}
