using System;
using System.Collections.Generic;

namespace MCB_DBlib.Models
{
	public partial class com_type
	{
		public com_type()
		{
			this.com_contact = new List<com_contact>();
		}

		public int id { get; set; }
		public string type { get; set; }
		public virtual ICollection<com_contact> com_contact { get; set; }
	}
}
