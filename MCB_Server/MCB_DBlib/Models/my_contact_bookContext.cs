using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;

using MCB_DBlib.Models.Mapping;

namespace MCB_DBlib.Models
{
	public partial class my_contact_bookContext : DbContext
	{
		static my_contact_bookContext()
		{
			Database.SetInitializer<my_contact_bookContext>(null);
		}

		public my_contact_bookContext()
			: base("Name=my_contact_bookContext")
		{

		}

		public DbSet<com_contact> com_contact { get; set; }
		public DbSet<com_type> com_type { get; set; }
		public DbSet<contact> contacts { get; set; }
		public DbSet<contact_companies> contact_companies { get; set; }

		public List<contact> contactList { get; private set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new com_contactMap());
			modelBuilder.Configurations.Add(new com_typeMap());
			modelBuilder.Configurations.Add(new contactMap());
			modelBuilder.Configurations.Add(new contact_companiesMap());
		}
	}
}
