using System;
using System.Collections.Generic;
using System.Text;

namespace MCB_DBlib.Models
{
	public partial class contact
	{
		public contact()
		{
			this.com_contact = new List<com_contact>();
		}

		public contact(string uq_name, string name, string forname)
		{
			this.com_contact = new List<com_contact>();
			this.uq_name = uq_name;
			this.Name = name;
			this.Forname = forname;
		}

		public int id { get; set; }
		public string uq_name { get; set; }
		public string Name { get; set; }
		public string Forname { get; set; }
		public Nullable<int> id_company { get; set; }
		public virtual ICollection<com_contact> com_contact { get; set; }
		public virtual contact_companies contact_company { get; set; }

		public override string ToString()
		{
			StringBuilder contactString = new StringBuilder();

			contactString.Append("Contact : ");

			contactString.Append("id = " + this.id);
			contactString.Append("\tuq_name = " + this.uq_name);
			if (this.Name != null)
				contactString.Append("\tName : " + this.Name);
			contactString.Append("\tForname : " + this.Forname);
			if (this.id_company != null)
			{
				contactString.Append("\tCompany id : " + this.id_company);
				contactString.Append("\tCompany Name : " + this.contact_company.Name);
			}

			contactString.AppendLine();

			return contactString.ToString();
		}
	}
}
