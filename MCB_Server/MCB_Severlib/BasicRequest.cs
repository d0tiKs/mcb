﻿using MCB_DBlib.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCB_ServerLib
{
	abstract class BasicRequest
	{
		#region Contacts
		public static void enumarateContact(my_contact_bookContext mcbCtx)
		{
			Console.WriteLine("#########\nEnumarate Contacts\n#########");
			foreach (contact ct in mcbCtx.contacts.ToList())
			{
				Console.Write(ct.ToString());
			}
		}

		static contact findContactByUQN(my_contact_bookContext mcbCtx, string uqn)
		{
			contact ct = null;

			try
			{
				ct = mcbCtx.contacts.First(c => c.uq_name == uqn);

				if (ct == null)
					throw new MissingFieldException();

				return ct;
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
				Console.ResetColor();
				return null;
			}
		}

		public static void addContact(my_contact_bookContext mcbCtx, contact ct)
		{
			try
			{
				mcbCtx.contacts.Add(ct);
				mcbCtx.SaveChanges();
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine("\n########\nAdding : {0},{1} Done\n########", ct.uq_name, ct.id);
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
			}
			Console.ResetColor();
		}

		public static void deleteContact(my_contact_bookContext mcbCtx, int id)
		{
			contact ct_to_del = null;
			try
			{
				ct_to_del = mcbCtx.contacts.Find(id);

				if (ct_to_del != null)
					mcbCtx.contacts.Remove(ct_to_del);

				mcbCtx.SaveChanges();
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine("\n##########\nDeleting : {0},{1} Done\n##########", ct_to_del.uq_name, ct_to_del.id);
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
			}
			Console.ResetColor();
		}
		public static void deleteContact(my_contact_bookContext mcbCtx, string uqn)
		{
			contact ct_to_del = null;
			try
			{
				ct_to_del = mcbCtx.contacts.First(c => c.uq_name == uqn);

				if (ct_to_del != null)
					mcbCtx.contacts.Remove(ct_to_del);

				mcbCtx.SaveChanges();
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine("\n##########\nDeleting : {0},{1} Done\n##########", ct_to_del.uq_name, ct_to_del.id);
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
			}
			Console.ResetColor();
		}
		#endregion

		#region Communication

		public enum ENUMcom_types
		{
			tel = 1,
			skype = 2,
			mail = 3,
			address = 4
		};

		public static void showComContact(my_contact_bookContext mcbCtx, int id)
		{
			contact ct;
			try
			{
				ct = mcbCtx.contacts.Find(id);
				Console.WriteLine("\n###########\nShow Com. : {0},{1}\n###########", ct.uq_name, ct.id);
				foreach (com_contact com in ct.com_contact)
					Console.Write(com.ToString());
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
				Console.ResetColor();
			}
		}
		public static void showComContact(my_contact_bookContext mcbCtx, string uqn)
		{
			contact ct;
			try
			{
				ct = mcbCtx.contacts.First(c => c.uq_name == uqn);
				Console.WriteLine("\n###########\nShow Com. : {0},{1}\n###########", ct.uq_name, ct.id);
				foreach (com_contact com in ct.com_contact)
					Console.Write(com.ToString());
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
				Console.ResetColor();
			}
		}

		public static void showSpecComContact(my_contact_bookContext mcbCtx, int id, ENUMcom_types type)
		{
			contact ct;
			List<com_contact> cm_ctl;
			try
			{
				ct = mcbCtx.contacts.Find(id);
				cm_ctl = mcbCtx.com_contact.Where(cm => cm.id_contact == ct.id && cm.id_com_type == (int)type).ToList();
				Console.WriteLine("\n#########\nShow Specific Com. : {0}:{1},{2}\n#########", type.ToString(), ct.uq_name, ct.id);
				foreach (com_contact cm_ct in cm_ctl)
					Console.Write(cm_ct.ToString());
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
				Console.ResetColor();
			}
		}
		public static void showSpecComContact(my_contact_bookContext mcbCtx, string uqn, ENUMcom_types type)
		{
			contact ct;
			List<com_contact> cm_ctl;
			try
			{
				ct = mcbCtx.contacts.First(c => c.uq_name == uqn);
				cm_ctl = mcbCtx.com_contact.Where(cm => cm.id_contact == ct.id && cm.id_com_type == (int)type)
																		.ToList();

				Console.WriteLine("\n#########\nShow Specific Com. : {0}:{1},{2}\n#########", type.ToString(), ct.uq_name, ct.id);
				foreach (com_contact cm_ct in cm_ctl)
					Console.Write(cm_ct.ToString());
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
				Console.ResetColor();
			}
		}
		#endregion

		public static void extractContactByCom_type(my_contact_bookContext mcbCtx, ENUMcom_types type)
		{
			Dictionary<string, string> dic_cmct = new Dictionary<string, string>();
			try
			{
				Console.WriteLine("\n#########\nExtract Contact by com_type : {0}\n#########", type.ToString());

				Dictionary<string,string> dic = (from ct in mcbCtx.contacts
																				 join cm_ct in mcbCtx.com_contact
																				 on ct.id equals cm_ct.id_contact
																				 where cm_ct.id_com_type == (int)type
																				 select new
																				 {
																					 uq_name = ct.uq_name,
																					 val = cm_ct.val
																				 }).ToDictionary(a => a.uq_name, b => b.val);

				/*Dictionary<string,string> dic = mcbCtx.contacts
																					.Join(mcbCtx.com_contact, c => c.id, cc => cc.id_contact, (c, cc) => new { c, cc })
																					.Where(a => a.cc.id_com_type == (int)type)
																					.Select(a => new
																					{
																						uq_name = a.c.uq_name,
																						val = a.cc.val
																					})
																					.ToDictionary(a => a.uq_name, b => b.val);*/


				foreach (var kvp in dic)
				{
					Console.WriteLine("{0} : {1}", kvp.Key, kvp.Value);
				}
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex);
				Console.ResetColor();
			}
		}
	}
}
